<?php

namespace Drupal\configelement\Page;

use Drupal\configelement\Form\ConfigFormFactory;
use Drupal\Core\Controller\FormController;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface;

/**
 * The config form controller.
 *
 * Usage in your routing file under defaults, add e.g. on of
 * _config_form: 'system.site'
 * _config_form: 'system.site:name'
 * _config_form:
 *   - 'system.site'
 *   - 'foo.bar'
 */
class ConfigFormController extends FormController {

  /**
   * The config form factory.
   *
   * @var \Drupal\configelement\Form\ConfigFormFactory
   */
  protected $configFormFactory;

  /**
   * ConfigFormPage constructor.
   *
   * @param \Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface $argument_resolver
   *   The argument resolver.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Drupal\configelement\Form\ConfigFormFactory $configFormFactory
   *   The config form factory.
   */
  public function __construct(ArgumentResolverInterface $argument_resolver, FormBuilderInterface $form_builder, ConfigFormFactory $configFormFactory) {
    parent::__construct($argument_resolver, $form_builder);
    $this->configFormFactory = $configFormFactory;
  }


  /**
   * @inheritDoc
   */
  protected function getFormArgument(RouteMatchInterface $route_match) {
    return $route_match->getRouteObject()->getDefault('_config_form');
  }

  /**
   * @inheritDoc
   */
  protected function getFormObject(RouteMatchInterface $route_match, $form_arg) {
    return $this->configFormFactory->formForConfig($form_arg);
  }

}
