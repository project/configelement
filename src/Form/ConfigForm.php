<?php

namespace Drupal\configelement\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ConfigForm
 *
 * @internal Use the factory.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * The config item (part of config object) names.
   *
   * @var string[]
   */
  protected $configItemNames;

  /**
   * ConfigForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param string[]|string $configItemNames
   *   The config item name or names. A config item name is either the name of
   *   a config object (like system.site) or the name of a config object,
   *   wit a config key separated by a colon (like system.site:name).
   */
  public function __construct(ConfigFactoryInterface $config_factory, $configItemNames) {
    parent::__construct($config_factory);
    $this->configItemNames = (array)$configItemNames;
  }

  private function toSnailCase($configName) {
    return str_replace(['.', ':'], ['__', '___'], $configName);
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    // We do not use the ::config so no need to return this. Distilling this
    // from config item names wold be possible but of no use here.
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return implode('____', array_map([$this, 'toSnailCase'], $this->configItemNames));
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    foreach ($this->configItemNames as $configItemName) {
      list($configName, $configKey) = explode(':', $configItemName) + ['1' => ''];
      $form['config'][$this->toSnailCase($configItemName)] = [
        '#type' => 'configelement_edit',
        '#config_name' => $configName,
        '#config_key' => $configKey,
      ];
    }
    return parent::buildForm($form, $form_state);
  }

}
