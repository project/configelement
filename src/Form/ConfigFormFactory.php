<?php

namespace Drupal\configelement\Form;

use Drupal\Core\Config\ConfigFactoryInterface;

class ConfigFormFactory {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * ConfigFormFactory constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->configFactory = $configFactory;
  }

  /**
   * Get a config form for said config items.
   *
   * @param string[]|string $configItemNames
   *   The config name or names.
   *
   * @return \Drupal\configelement\Form\ConfigForm
   *   The config form.
   */
  public function formForConfig($configItemNames) {
    return new ConfigForm($this->configFactory, $configItemNames);
  }

}
