<?php

namespace Drupal\configelement\EditableConfig;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\Schema\ArrayElement;
use Drupal\Core\Config\Schema\Mapping;
use Drupal\Core\Config\Schema\TypedConfigInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;

abstract class EditableConfigWrapperBase implements EditableConfigWrapperInterface {

  /** @var \Drupal\Core\Config\Config | \Drupal\language\Config\LanguageConfigOverride */
  protected $config;

  /** @var \Drupal\Core\Config\TypedConfigManagerInterface */
  protected $typedConfigManager;

  /**
   * EditableConfigWrapper constructor.
   *
   * @internal Use EditableConfigItemFactory::get
   *
   * @param \Drupal\Core\Config\Config | \Drupal\language\Config\LanguageConfigOverride $config
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   */
  public function __construct($config, TypedConfigManagerInterface $typedConfigManager) {
    $this->config = $config;
    $this->typedConfigManager = $typedConfigManager;
    $this->populateMappings();
  }

  /**
   * {@inheritDoc}
   */
  public function get($key) {
    return $this->config->get($key);
  }

  /**
   * {@inheritDoc}
   */
  public function set($key, $value) {
    if ($key) {
      $this->config->set($key, $value);
    }
    else {
      $this->config->setData($value);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function has($key) {
    try {
      $this->getSchemaWrapper()->get($key);
    } catch (\InvalidArgumentException $e) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function save() {
    $this->unpopulateMappings();
    if ($this->config->get() !== $this->getOriginalData()) {
      $this->config->save();
    }
    $this->populateMappings();
  }

  /**
   * Get original data.
   *
   * @return mixed
   */
  abstract protected function getOriginalData();

  /**
   * {@inheritDoc}
   */
  public function getSchemaWrapper($propertyPath = '') {
    if ($propertyPath) {
      $parents = explode('.', $propertyPath);
      if ($parents === ['']) {
        $parents = [];
      }
      if (!NestedArray::keyExists($this->getConfigData(), $parents)) {
        $savedConfigData = $this->getConfigData();
        $this->config->set($propertyPath, '');
      }
    }
    $schemaWrapper = $this->typedConfigManager->createFromNameAndData($this->config->getName(), $this->getConfigData());
    if (!$schemaWrapper instanceof TypedConfigInterface) {
      throw new \InvalidArgumentException(sprintf('Config %s does not have a schema.', $this->config->getName()));
    }
    if ($propertyPath) {
      $schemaWrapper = $schemaWrapper->get($propertyPath);
      if (isset($savedConfigData)) {
        $this->config->setData($savedConfigData);
      }
    }
    return $schemaWrapper;
  }

  /**
   * Get config data.
   *
   * @return array
   */
  abstract protected function getConfigData();


  /**
   * {@inheritDoc}
   */
  public function validate() {
    return $this->getSchemaWrapper()->validate();
  }

  /**
   * Populate the mappings.
   *
   * @todo Document this.
   *
   * @param string $propertyPath
   */
  private function populateMappings($propertyPath = '') {
    $parents = explode('.', $propertyPath);
    if ($parents === ['']) {
      $parents = [];
    }
    $schemaWrapper = $this->getSchemaWrapper($propertyPath);
    if ($schemaWrapper instanceof ArrayElement) {
      if ($schemaWrapper instanceof Mapping) {
        $definition = $schemaWrapper->getDataDefinition();
        foreach ($definition['mapping'] as $key => $itemDefinition) {
          $itemParents = array_merge($parents, [$key]);
          if (!NestedArray::keyExists($this->getConfigData(), $itemParents)) {
            $this->populateMappings(implode('.', $itemParents));
          }
        }
      }
      else {
        foreach ($schemaWrapper->getElements() as $key => $element) {
          $this->populateMappings("$propertyPath.$key");
        }
      }
    }
    else {
      $definition = $schemaWrapper->getDataDefinition();
      if (
        !NestedArray::keyExists($this->getConfigData(), $parents)
        && $this->relevantMappingDefinition($definition)
      ) {
        $this->config->set($propertyPath, '');
      }
    }
  }

  /**
   * Unpopulate the mappings.
   *
   * @param string $propertyPath
   */
  private function unpopulateMappings($propertyPath = '') {
    $parents = explode('.', $propertyPath);
    if ($parents === ['']) {
      $parents = [];
    }
    $schemaWrapper = $this->getSchemaWrapper($propertyPath);
    if ($schemaWrapper instanceof ArrayElement) {
      foreach ($schemaWrapper->getElements() as $key => $element) {
        $itemParents = array_merge($parents, [$key]);
        $this->unpopulateMappings(implode('.', $itemParents));
      }
    }
    else {
      $data = $this->getConfigData();
      if (is_null(NestedArray::getValue($data, $parents))) {
        NestedArray::unsetValue($data, $parents);
        $this->config->setData($data);
      }
    }
  }

  /**
   * Is relevant mapping definition.
   *
   * @param $definition
   *
   * @return bool
   */
  protected function relevantMappingDefinition($definition) {
    return empty($definition['internal']) && empty($definition['computed']);
  }

}
